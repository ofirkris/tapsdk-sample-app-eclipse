package pm.tap.sdk.sample;

import pm.tap.sdk.Tap;
import pm.tap.sdk.libraries.interfaces.IEventDelegate;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;


public class MainActivity extends Activity implements IEventDelegate{

	private final static String TAG = "tapSDK";
	private Tap tap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Tap.establish(getBaseContext());
		
		this.tap = new Tap(this, "EXAMPLE_UNITID"); //Testing UnitID, replace with your UnitID.
		final IEventDelegate listener = this;
		
		this.tap.setNotificationEnable(true);
		
		findViewById(R.id.more_games).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				tap.showMoreGamesWithViewControllerAndAppID(listener);
			}
			
		});
		
		findViewById(R.id.properties).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				try{
					tap.setProperties("#4754AC", "#406491", "#4794AC");
					tap.showMoreGamesWithViewControllerAndAppID(listener, "Customize splash", "Customize header asdasdsad as");
					
					//Restart the values for the next tests.
					tap.setProperties(null, null, null);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
				
			}
			
		});
		
		
		findViewById(R.id.interstitial).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				try{
					tap.InterstitialWithViewControllerAndAppID(listener);
					
                }
                catch (Exception e){
                    e.printStackTrace();
                }
				
			}
			
		});
		
		findViewById(R.id.game_slug).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				try{
					tap.showGameBySlug(listener, "zombies-td");
					
                }
                catch (Exception e){
                    e.printStackTrace();
                }
				
			}
			
		});
		
		
		findViewById(R.id.offers_wall).setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				try{
					tap.showOffersWall(listener, "TapCoins", 1);
					
                }
                catch (Exception e){
                    e.printStackTrace();
                }
				
			}
			
		});
			
	}
	
	@Override
    public void viewDidAppear() {
		Log.i(TAG , "viewDidAppear");
    }

    @Override
    public void viewDidDisappear() {
    	Log.i(TAG , "viewDidDisappear");
    }

    @Override
    public void viewConnectFail(String s) {
    	Log.i(TAG , "viewConnectFail: " + s);
    }

    @Override
    public void conversion() {
    	Log.i(TAG , "conversion");
    }
    
    @Override
	public void onOfferComplete(int points) {
		Log.i(TAG, "onOfferComplete: " + points);
		Toast.makeText(this, "onOfferComplete: " + points, Toast.LENGTH_LONG).show();
	}
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    	
    	try{
    		tap.onResume();
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    @Override
    protected void onPause()
    {
    	try{
    		tap.onPause();
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	super.onPause();
    }
    
    @Override
    protected void onDestroy()
    {
    	try{
    		tap.onDestroy();
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	super.onDestroy();
    }
	
}
